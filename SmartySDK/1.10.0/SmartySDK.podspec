Pod::Spec.new do |s|

  s.name         = "SmartySDK"
  s.version      = "1.10.0"
  s.summary      = "Smarty Game Server client SDK"
  s.homepage     = "http://smarty.lite.games"

  s.license      = {
    :type => "Copyright",
    :text => "Copyright 2019 LITE Games GmbH"
  }
  s.author = "LITE Games"

  s.platform = :ios, "12.0"

  s.source = {
    :git => "https://bitbucket.org/litegames/smarty-ios-sdk-packages.git",
    :tag => "1.10.0"
  }


  s.vendored_frameworks = "SmartySDK.xcframework"

  s.dependency 'SFS2XAPI', '= 1.7.15'
  s.dependency 'FBSDKCoreKit', '~> 17.0'
  s.dependency 'FBSDKLoginKit', '~> 17.0'
  s.dependency 'FBSDKShareKit', '~> 17.0'
  s.dependency 'SDWebImage', '~> 5.0'

end
