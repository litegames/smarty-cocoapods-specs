#
#  Be sure to run `pod spec lint SmartySDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # Description
  spec.name         = "SmartySDK"
  spec.version      = "1.7.0"
  spec.summary      = "Smarty Game Server client SDK"
  spec.homepage     = "http://smarty.lite.games"

  # License
  spec.license      = {
    :type => "Copyright",
    :text => "Copyright 2019 LITE Games GmbH"
  }

  # Author
  spec.author = "Marek Zdankiewicz"

  # Platform
  spec.platform = :ios, "9.0"

  # Swift
  spec.swift_versions = ["5.0"]

  # Source
  spec.source = {
    :git => "https://bitbucket.org/litegames/smarty-ios-sdk-packages.git",
    :tag => "#{spec.version}"
  }


  # Source code
  spec.vendored_frameworks =
    "SmartySDK.framework",
    "SFS2XAPIIOS.framework"

  # Resources
  spec.resources = 'SmartySDKResources.bundle'

  # Project settings
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  spec.dependency 'FBSDKCoreKit', '~> 9.0'
  spec.dependency 'FBSDKLoginKit', '~> 9.0'
  spec.dependency 'FBSDKShareKit', '~> 9.0'
  spec.dependency 'SDWebImage', '~> 5.0'

end
