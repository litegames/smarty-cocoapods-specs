Pod::Spec.new do |s|
  s.name             = 'SFS2XAPI'
  s.version          = '1.7.15'
  s.summary          = 'SmartFox server client for iOS.'
  s.description      = <<-DESC
    Release notes: https://www.smartfoxserver.com/download/releaseNotes/SFS2X_API_ObjectiveC.txt
  DESC

  s.homepage         = 'https://www.smartfoxserver.com'
  s.license          = {
    :type => "Copyright",
    :text => "Copyright GotoAndPlay S.N.C"
  }
  s.author           = "GotoAndPlay S.N.C"
  s.source           = {
    :http => 'https://www.smartfoxserver.com/downloads/sfs2x/api/SFS2X_API_ObjectiveC_v1.7.15.zip',
    :sha1 => '50a34777b1bbf03c11ae120de886f96f644b8267'
  }

  s.platform = :ios
  s.ios.deployment_target = '10.0'

  s.vendored_frameworks = [
    "SFS2X_API_ObjectiveC_1.7.15/SFS2XAPIIOS.xcframework",
  ]
end
